public class ReportingOperateur {


	
	private String nom;		
	
	private String type;	
	
	private double prix;
	
    private String lieu;
	
	private String date ;
	
	private String frequence ;
	
	private String id_contrat ;


	public ReportingOperateur(String nom, String type, double prix, String lieu, String date, String frequence,
			String id_contrat) {
		
		this.nom = nom;
		this.type = type;
		this.prix = prix;
		this.lieu = lieu;
		this.date = date;
		this.frequence = frequence;
		this.id_contrat = id_contrat;
	}
	



	public String getNom() {
		return nom;
	}



	public void setNom(String nom) {
		this.nom = nom;
	}



	public String getType() {
		return type;
	}



	public void setType(String type) {
		this.type = type;
	}



	public double getPrix() {
		return prix;
	}



	public void setPrix(double prix) {
		this.prix = prix;
	}



	public String getLieu() {
		return lieu;
	}



	public void setLieu(String lieu) {
		this.lieu = lieu;
	}



	public String getDate() {
		return date;
	}



	public void setDate(String date) {
		this.date = date;
	}



	public String getFrequence() {
		return frequence;
	}



	public void setFrequence(String frequence) {
		this.frequence = frequence;
	}



	public String getId_contrat() {
		return id_contrat;
	}



	public void setId_contrat(String id_contrat) {
		this.id_contrat = id_contrat;
	}




	

	
	public String toString() {
		return "Contrat  : ID : " + id_contrat +   " \n Nom: " + nom
				+ " \n Prix: " + prix + "\n Lieu: " + lieu + "\n Date:"+date+"\n Frequence:"+frequence ;
	}

}
