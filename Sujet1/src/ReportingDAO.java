import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ReportingDAO {
	final static String URL = "jdbc:oracle:thin:@localhost:1521:xe";
	final static String LOGIN = "SYSTEM";  //exemple BDD1
	final static String PASS = "manete95";   //exemple BDD1

	
	/**
	 * Constructeur de la classe
	 * 
	 */
	public ReportingDAO() {
		// chargement du pilote de bases de données
		try {
			Class.forName("oracle.jdbc.OracleDriver");
		} catch (ClassNotFoundException e) {
			System.err
					.println("Impossible de charger le pilote de BDD, ne pas oublier d'importer le fichier .jar dans le projet");
		}

	}

	/**
	 * Permet d'ajouter un Reporting dans la table Reporting Le mode est auto-commit
	 * par défaut : chaque insertion est validée
	 * 
	 * @param Reporting
	 *            l'Reporting à ajouter
	 * @return retourne le nombre de lignes ajoutées dans la table
	 */
	public int ajouter(Reporting Reporting) {
		Connection con = null;
		PreparedStatement ps = null;
		int retour = 0;

		// connexion à la base de données
		try {

			// tentative de connexion
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			// préparation de l'instruction SQL, chaque ? représente une valeur
			// à communiquer dans l'insertion
			// les getters permettent de récupérer les valeurs des attributs
			// souhaités
			ps = con.prepareStatement("INSERT INTO Reporting (rpt_id_reporting,rpt_nom,rpt_frequence, rpt_lieu, rpt_date,rpt_type ) VALUES ( ?, ?, ?,?,?,?)");
			ps.setString(1,Reporting.getId_reporting());
			ps.setString(2, Reporting.getNom());
			ps.setString(3,Reporting.getFrequence());
			ps.setString(4, Reporting.getLieu());
			ps.setString(5,Reporting.getDate());
			ps.setString(6, Reporting.getType());
			
			

			// Exécution de la requête
			retour = ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// fermeture du preparedStatement et de la connexion
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return retour;

	}

		public Reporting getReporting(int i) {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Reporting retour = null;

		// connexion à la base de données
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM Reporting WHERE rpt_id_reporting = ?");
			ps.setInt(1, i);

			
			rs = ps.executeQuery();
			// passe à la première (et unique) ligne retournée
			if (rs.next())
				retour = new Reporting(rs.getString("rpt_id_reporting"),rs.getString("rpt_nom"),
						rs.getString("rpt_frequence"), rs.getString("rpt_lieu"),rs.getString("rpt_date"),rs.getString("rpt_type"));

		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			// fermeture du ResultSet, du PreparedStatement et de la Connexion
			try {
				if (rs != null)
					rs.close();
			} catch (Exception ignore) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return retour;

	}

	/**
	 * Permet de récupérer tous les Reportings stockés dans la table Reporting
	 * 
	 * @return une ArrayList d'Reportings
	 */
	public List<Reporting> getListeReportings() {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Reporting> retour = new ArrayList<Reporting>();

		// connexion à la base de données
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM Reporting");

			// on exécute la requête
			rs = ps.executeQuery();
			// on parcourt les lignes du résultat
			while (rs.next())
				retour.add(new Reporting(rs.getString("rpt_id_reporting"),rs.getString("rpt_nom"),
						rs.getString("rpt_frequence"), rs.getString("rpt_lieu"),rs.getString("rpt_date"),rs.getString("rpt_type")));

		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			
			try {
				if (rs != null)
					rs.close();
			} catch (Exception ignore) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return retour;

	}

	// main permettant de tester la classe
	public static void main(String[] args) throws SQLException {

		ReportingDAO ReportingDAO = new ReportingDAO();
		// test de la méthode ajouter
		Reporting a1 = new Reporting("Djeko", "Lambo","Neully","01 Avril 2017","14D1","1EA7");
		int retour = ReportingDAO.ajouter(a1);

		System.out.println(retour + " Reporting Ajout� ");

		// test de la méthode getReporting
		Reporting a2 = ReportingDAO.getReporting(1);
		System.out.println(a2);

		// test de la méthode getListeReportings
		List<Reporting> liste = ReportingDAO.getListeReportings();
		// affichage des Reportings
		for (Reporting art : liste) {
			System.out.println(art.toString());
		}

	}


}
