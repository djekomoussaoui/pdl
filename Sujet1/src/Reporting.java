public class Reporting {


	
	private String nom;		
	
	private String type;	
	
    private String lieu;
	
	private String date ;
	
	private String frequence ;
	
	private String id_reporting ;


	public Reporting(String nom, String type,  String lieu, String date, String frequence,
			String id_reporting) {
		
		this.nom = nom;
		this.type = type;
		this.lieu = lieu;
		this.date = date;
		this.frequence = frequence;
		this.id_reporting = id_reporting;
	}
	



	public String getNom() {
		return nom;
	}



	public void setNom(String nom) {
		this.nom = nom;
	}



	public String getType() {
		return type;
	}



	public void setType(String type) {
		this.type = type;
	}




	public String getLieu() {
		return lieu;
	}



	public void setLieu(String lieu) {
		this.lieu = lieu;
	}



	public String getDate() {
		return date;
	}



	public void setDate(String date) {
		this.date = date;
	}



	public String getFrequence() {
		return frequence;
	}



	public void setFrequence(String frequence) {
		this.frequence = frequence;
	}



	public String getId_reporting() {
		return id_reporting;
	}



	public void setId_reporting(String id_reporting) {
		this.id_reporting = id_reporting;
	}




	

	
	public String toString() {
		return "Reporting  : ID : " + id_reporting +   " \n Nom: " + nom
				+  "\n Lieu: " + lieu + "\n Date:"+date+"\n Frequence:"+frequence ;
	}

}
