import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ContratDAO {
	final static String URL = "jdbc:oracle:thin:@localhost:1521:xe";
	final static String LOGIN = "SYSTEM";  //exemple BDD1
	final static String PASS = "manete95";   //exemple BDD1

	
	/**
	 * Constructeur de la classe
	 * 
	 */
	public ContratDAO() {
		// chargement du pilote de bases de données
		try {
			Class.forName("oracle.jdbc.OracleDriver");
		} catch (ClassNotFoundException e) {
			System.err
					.println("Impossible de charger le pilote de BDD, ne pas oublier d'importer le fichier .jar dans le projet");
		}

	}

	/**
	 * Permet d'ajouter un Contrat dans la table Contrat Le mode est auto-commit
	 * par défaut : chaque insertion est validée
	 * 
	 * @param Contrat
	 *            l'Contrat à ajouter
	 * @return retourne le nombre de lignes ajoutées dans la table
	 */
	public int ajouter(Contrat Contrat) {
		Connection con = null;
		PreparedStatement ps = null;
		int retour = 0;

		// connexion à la base de données
		try {

			// tentative de connexion
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			// préparation de l'instruction SQL, chaque ? représente une valeur
			// à communiquer dans l'insertion
			// les getters permettent de récupérer les valeurs des attributs
			// souhaités
			ps = con.prepareStatement("INSERT INTO Contrat (art_id_contrat,art_nom,  art_prix,art_frequence, art_lieu, art_date,art_type ) VALUES (?, ?, ?, ?,?,?,?)");
			ps.setString(1,Contrat.getId_contrat());
			ps.setString(2, Contrat.getNom());
			ps.setDouble(3, Contrat.getPrix());
			ps.setString(4,Contrat.getFrequence());
			ps.setString(5, Contrat.getLieu());
			ps.setString(6,Contrat.getDate());
			ps.setString(7, Contrat.getType());
			
			

			// Exécution de la requête
			retour = ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// fermeture du preparedStatement et de la connexion
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return retour;

	}

		public Contrat getContrat(int i) {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Contrat retour = null;

		// connexion à la base de données
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM Contrat WHERE art_id_contrat = ?");
			ps.setInt(1, i);

			
			rs = ps.executeQuery();
			// passe à la première (et unique) ligne retournée
			if (rs.next())
				retour = new Contrat(rs.getString("art_id_contrat"),rs.getString("art_nom"),
						rs.getDouble("art_prix"),rs.getString("art_frequence"), rs.getString("art_lieu"),rs.getString("art_date"),rs.getString("art_type"));

		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			// fermeture du ResultSet, du PreparedStatement et de la Connexion
			try {
				if (rs != null)
					rs.close();
			} catch (Exception ignore) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return retour;

	}

	/**
	 * Permet de récupérer tous les Contrats stockés dans la table Contrat
	 * 
	 * @return une ArrayList d'Contrats
	 */
	public List<Contrat> getListeContrats() {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Contrat> retour = new ArrayList<Contrat>();

		// connexion à la base de données
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM Contrat");

			// on exécute la requête
			rs = ps.executeQuery();
			// on parcourt les lignes du résultat
			while (rs.next())
				retour.add(new Contrat((rs.getString("art_id_contrat")),rs.getString("art_nom"),
						rs.getDouble("art_prix"),rs.getString("art_frequence"), rs.getString("art_lieu"),rs.getString("art_date"),rs.getString("art_type")));

		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			
			try {
				if (rs != null)
					rs.close();
			} catch (Exception ignore) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return retour;

	}

	// main permettant de tester la classe
	public static void main(String[] args) throws SQLException {

		ContratDAO ContratDAO = new ContratDAO();
		// test de la méthode ajouter
		Contrat a1 = new Contrat("Djeko", "Lambo", 149.9,"Neully","01 Avril 2017","14D1","1EA7");
		int retour = ContratDAO.ajouter(a1);

		System.out.println(retour + " Contrat Ajout� ");

		// test de la méthode getContrat
		Contrat a2 = ContratDAO.getContrat(1);
		System.out.println(a2);

		// test de la méthode getListeContrats
		List<Contrat> liste = ContratDAO.getListeContrats();
		// affichage des Contrats
		for (Contrat art : liste) {
			System.out.println(art.toString());
		}

	}

}
