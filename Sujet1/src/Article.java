
/**
 * Classe Article
 * @author grave - roueche - serais
 * @version 1.2
 * */

public class Article {

	/** 
	 * référence de l'article
	 */
	private int IdMaintenance;		
	/**
	 * désignation
	 */
	private String debut;	
	/**
	 * prix unitaire hors taxe
	 */
	private String type;		
	/**
	 * quantité en stock
	 */
	private String nom;


	/**
	 * Constructeur
	 * @param IdMaintenance référence de l'article
	 * @param debut désignation
	 * @param type prix unitaire hors taxe
	 * @param nom quantité en stock
	 */
	public Article(int IdMaintenance, String debut, String type, String nom) {
		this.IdMaintenance=IdMaintenance;
		this.debut = debut;
		this.type = type;
		this.nom = nom;
	}
	
	/**
	 * getter pour l'attribut IdMaintenance
	 * @return valeur de la IdMaintenance article
	 */
	public int getIdMaintenance() {
		return IdMaintenance;
	}
	/**
	 * getter pour l'attribut désignation
	 * @return valeur de la désignation article
	 */
	public String getDebut() {
		return debut;
	}
	/**
	 * setter  pour l'attribut debut
	 * @param debut : nouvelle valeur de la désignation article
	 */
	public void setdebut(String debut) {
		this.debut = debut;
	}
	/**
	 * getter  pour l'attribut type
	 * @return valeur de prix unitaire HT
	 */
	public String getType() {
		return type;
	}
	/**
	 * setter  pour l'attribut type
	 * @param type :  nouvelle valeur de prix unitaire HT
	 */
	public void settype(String type) {
		this.type = type;
	}
	/**
	 * getter  pour l'attribut nom
	 * @return valeur de quantité en stock
	 */
	public String getNom() {
		return nom;
	}
	/**
	 * setter  pour l'attribut nom
	 * @param nom : nouvelle valeur de prix unitaire HT
	 */
	public void setnom(String nom) {
		this.nom = nom;
	}

	/**
	 * Redéfinition de la méthode toString permettant de définir la traduction de l'objet en String
	 * pour l'affichage par exemple
	 */
	public String toString() {
		return "Article [réf : " + IdMaintenance + " - " + debut
				+ ", " + type + "€ HT, " + nom + " en stock]";
	}
}
