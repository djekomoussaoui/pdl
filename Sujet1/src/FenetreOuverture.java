
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JPanel;
import javax.swing.BoxLayout;
import javax.swing.Box;

import java.util.List;


/**
 * Classe ArticleFenetre
 * Definit et ouvre une fenetre qui :
 * 
 *    - Permet l'insertion d'un nouvel article dans la table article via
 * la saisie des valeurs de reference, designation, prix et quantite en stock
 *    - Permet l'affichage de tous les articles dans la console
 * @author grave - roueche - serais
 * @version 1.3
 * */


public class FenetreOuverture extends JFrame implements ActionListener {
	/**
	 * numero de version pour classe serialisable Permet d'eviter le warning
	 * "The serializable class ArticleFenetre does not declare a static final serialVersionUID field of type long"
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * conteneur : il accueille les differents composants graphiques de
	 * ArticleFenetre
	 */
	private JPanel containerPanel;

	/**
	 * zone de texte pour le champ reference
	 */
	private JTextField textFieldId;

	/**
	 * zone de texte pour le champ designation
	 */
	private JTextField textFieldMotDePasse;

	

	/**
	 * label reference
	 */
	private JLabel labelIdentifiant;

	/**
	 * label designation
	 */
	private JLabel labelMotDePasse;

	/**
	 * label prix unitaire hors taxe
	 */
	
	/**
	 * bouton d'ajout de l'article
	 */
	private JButton boutonConnexion;

	
	/**
	 * Constructeur Definit la fenetre et ses composants - affiche la fenetre
	 */
	public FenetreOuverture() {
		// on instancie la classe Article DAO
		

		// on fixe le titre de la fenetre
		this.setTitle("IDENTIFICATION");
		// initialisation de la taille de la fenetre
		this.setSize(400, 400);

		// creation du conteneur
		containerPanel = new JPanel();

		// choix du Layout pour ce conteneur
		// il permet de gerer la position des elements
		// il autorisera un retaillage de la fenetre en conservant la
		// presentation
		// BoxLayout permet par exemple de positionner les elements sur une
		// colonne ( PAGE_AXIS )
		containerPanel.setLayout(new BoxLayout(containerPanel,BoxLayout.PAGE_AXIS));

		// choix de la couleur pour le conteneur
		containerPanel.setBackground(Color.WHITE);

		// instantiation des composants graphiques
		textFieldId = new JTextField();
		textFieldMotDePasse = new JTextField();
		
		boutonConnexion = new JButton("Connexion");
		labelIdentifiant = new JLabel("Votre Identifiant :");
		labelMotDePasse = new JLabel("Mot de passe :");
		

		
		containerPanel.add(Box.createRigidArea(new Dimension(0, 100)));
		// ajout des composants sur le container
		containerPanel.add(labelIdentifiant);
		// introduire une espace constant entre le label et le champ texte
		containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		containerPanel.add(textFieldId);
		// introduire une espace constant entre le champ texte et le composant
		// suivant
		containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));

		containerPanel.add(labelMotDePasse);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		containerPanel.add(textFieldMotDePasse);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 50)));


		containerPanel.add(boutonConnexion);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 20)));

		
		

		// ajouter une bordure vide de taille constante autour de l'ensemble des
		// composants
		containerPanel.setBorder(BorderFactory
				.createEmptyBorder(10, 10, 10, 10));

		// ajout des ecouteurs sur les boutons pour gerer les evenements
		boutonConnexion.addActionListener(this);
		

		// permet de quitter l'application si on ferme la fenetre
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		this.setContentPane(containerPanel);

		// affichage de la fenetre
		this.setVisible(true);
	}

	/**
	 * Gere les actions realisees sur les boutons
	 *
	 */
	public void actionPerformed(ActionEvent ae) {
		int retour; // code de retour de la classe ArticleDAO

		try {
			if (ae.getSource() == boutonConnexion) {
				// on ferme la premiere fenetre
				this.setVisible(false);
				// ouverture de la nouvelle fenetre
				new FenetreMenu();
				
			} 
			
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this,
					"Veuillez controler vos saisies", "Erreur",
					JOptionPane.ERROR_MESSAGE);
			System.err.println("Veuillez controler vos saisies");
		}

	}

	public static void main(String[] args) {
		new FenetreOuverture();
	}

}
