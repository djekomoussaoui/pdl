import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class FenetreCreerContratDAO {

	/**
	 * Paramètres de connexion à la base de données oracle URL, LOGIN et PASS
	 * sont des constantes
	 */
	final static String URL = "jdbc:oracle:thin:@localhost:1521:xe";
	final static String LOGIN = "****";  //exemple BDD1
	final static String PASS = "****";   //exemple BDD1

	
	/**
	 * Constructeur de la classe
	 * 
	 */
	public FenetreCreerContratDAO() {
		// chargement du pilote de bases de données
		try {
			Class.forName("oracle.jdbc.OracleDriver");
		} catch (ClassNotFoundException e) {
			System.err
					.println("Impossible de charger le pilote de BDD, ne pas oublier d'importer le fichier .jar dans le projet");
		}

	}

	/**
	 * Permet d'ajouter un Contrat dans la table Contrat Le mode est auto-commit
	 * par défaut : chaque insertion est validée
	 * 
	 * @param Contrat
	 *            l'Contrat à ajouter
	 * @return retourne le nombre de lignes ajoutées dans la table
	 */
	public int ajouter(Contrat Contrat) {
		Connection con = null;
		PreparedStatement ps = null;
		int retour = 0;

		// connexion à la base de données
		try {

			// tentative de connexion
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			// préparation de l'instruction SQL, chaque ? représente une valeur
			// à communiquer dans l'insertion
			// les getters permettent de récupérer les valeurs des attributs
			// souhaités
			ps = con.prepareStatement("INSERT INTO Contrat (art_reference, art_designation, art_pu_ht, art_qte_stock) VALUES (?, ?, ?, ?)");
			ps.setString(1, Contrat.getId_contrat());
			ps.setString(2, Contrat.getDate());
			ps.setString(3, Contrat.getType());
			ps.setString(4, Contrat.getNom());

			// Exécution de la requête
			retour = ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// fermeture du preparedStatement et de la connexion
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return retour;

	}

	/**
	 * Permet de récupérer un Contrat à partir de sa référence
	 * 
	 * @param reference
	 *            la référence de l'Contrat à récupérer
	 * @return 	l'Contrat trouvé;
	 * 			null si aucun Contrat ne correspond à cette référence
	 */
	public Contrat getContrat(int reference) {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Contrat retour = null;

		// connexion à la base de données
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM Contrat WHERE art_reference = ?");
			ps.setInt(1, reference);

			// on exécute la requête
			// rs contient un pointeur situé juste avant la première ligne
			// retournée
			rs = ps.executeQuery();
			// passe à la première (et unique) ligne retournée
			if (rs.next())
				retour = new Contrat(rs.getString("art_reference"),
						rs.getString("art_designation"),
						rs.getString("art_pu_ht"), rs.getString("art_qte_stock"));

		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			// fermeture du ResultSet, du PreparedStatement et de la Connexion
			try {
				if (rs != null)
					rs.close();
			} catch (Exception ignore) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return retour;

	}

	/**
	 * Permet de récupérer tous les Contrats stockés dans la table Contrat
	 * 
	 * @return une ArrayList d'Contrats
	 */
	public List<Contrat> getListeContrats() {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Contrat> retour = new ArrayList<Contrat>();

		// connexion à la base de données
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM Contrat");

			// on exécute la requête
			rs = ps.executeQuery();
			// on parcourt les lignes du résultat
			while (rs.next())
				retour.add(new Contrat(rs.getInt("art_reference"), rs
						.getString("art_designation"), rs
						.getString("art_pu_ht"), rs.getString("art_qte_stock")));

		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			// fermeture du rs, du preparedStatement et de la connexion
			try {
				if (rs != null)
					rs.close();
			} catch (Exception ignore) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return retour;

	}

	// main permettant de tester la classe
	public static void main(String[] args) throws SQLException {

		FenetreCreerContratDAO FenetreCreerContratDAO = new FenetreCreerContratDAO();
		// test de la méthode ajouter
		Contrat a1 = new Contrat(1, "Set de 2 raquettes de ping-pong", "149.9","10");
		int retour = FenetreCreerContratDAO.ajouter(a1);

		System.out.println(retour + " lignes ajoutées");

		// test de la méthode getContrat
		Contrat a2 = FenetreCreerContratDAO.getContrat(1);
		System.out.println(a2);

		// test de la méthode getListeContrats
		List<Contrat> liste = FenetreCreerContratDAO.getListeContrats();
		// affichage des Contrats
		for (Contrat art : liste) {
			System.out.println(art.toString());
		}

	}
}