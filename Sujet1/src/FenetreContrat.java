
    import java.awt.Color;

	import java.awt.event.ActionEvent;
	import java.awt.event.ActionListener;
	import java.awt.Dimension;

	import javax.swing.BorderFactory;
	import javax.swing.JButton;
	import javax.swing.JFrame;
	import javax.swing.JLabel;
	import javax.swing.JOptionPane;
	import javax.swing.JScrollPane;
	import javax.swing.JTextArea;
	import javax.swing.JTextField;
	import javax.swing.JPanel;
	import javax.swing.BoxLayout;
	import javax.swing.Box;

	import java.util.List;


	/**
	 * Classe ContratFenetre
	 * Definit et ouvre une fenetre qui :
	 * 
	 *    - Permet l'insertion d'un nouvel Contrat dans la table Contrat via
	 * la saisie des valeurs de Lieu, designation, prix et quantite en stock
	 *    - Permet l'affichage de tous les Contrats dans la console
	 * @author grave - roueche - serais
	 * @version 1.3
	 * */


	public class FenetreContrat extends JFrame implements ActionListener {
		/**
		 * numero de version pour classe serialisable Permet d'eviter le warning
		 * "The serializable class ContratFenetre does not declare a static final serialVersionUID field of type long"
		 */
		private static final long serialVersionUID = 1L;

		/**
		 * conteneur : il accueille les differents composants graphiques de
		 * ContratFenetre
		 */
		
		// creation du conteneur
		private JPanel containerPanel;
		
		// creation des boutons
		
		private JTextField textFieldNom;
		
		private JTextField textFieldType;

		private JTextField textFieldPrix;

		private JTextField textFieldFrequence;
		
		private JTextField textFieldLieu;
		
		private JTextField textFieldDate;
		
		private JTextField textFieldID;
		
		
		
		private JLabel labelNom;
		
		private JLabel labelType;

		private JLabel labelPrix;

		private JLabel labelFrequence;
		
		private JLabel labelLieu;
		
		private JLabel labelDate;
		
		private JLabel labelID;
		
		private JButton boutonValider;

		private JButton boutonAnnuler;
		
		private JButton boutonRetour;
		
		

		/**
		 * instance de ContratDAO permettant les acces a la base de donnees
		 */
		private ContratDAO monContratDAO;

		/**
		 * Constructeur Definit la fenetre et ses composants - affiche la fenetre
		 */
		public FenetreContrat() {
			// on instancie la classe Contrat DAO
			
			this.monContratDAO = new ContratDAO();
			

			// on fixe le titre de la fenetre
			this.setTitle("Cr�ation Contrat");
			// initialisation de la taille de la fenetre
			this.setSize(800, 800);

			// creation du conteneur
			containerPanel = new JPanel();

			// choix du Layout pour ce conteneur
			// il permet de gerer la position des elements
			// il autorisera un retaillage de la fenetre en conservant la
			// presentation
			// BoxLayout permet par exemple de positionner les elements sur une
			// colonne ( PAGE_AXIS )
			containerPanel.setLayout(new BoxLayout(containerPanel,BoxLayout.PAGE_AXIS));

			// choix de la couleur pour le conteneur
			containerPanel.setBackground(Color.WHITE);

			// instantiation des composants graphiques
			textFieldNom = new JTextField();
			textFieldPrix = new JTextField();
			textFieldFrequence = new JTextField();
			textFieldLieu = new JTextField();
			textFieldDate = new JTextField();
			textFieldType = new JTextField();
			textFieldID = new JTextField();
			
			boutonValider = new JButton("Valider la saisie");
			boutonAnnuler = new JButton("Annuler la saisie");
			boutonRetour = new JButton("Retour au menu");
			labelNom = new JLabel("Nom Operateur  :");
			labelPrix = new JLabel("Prix :");
			labelFrequence = new JLabel("Frequence de maintenance :");
			labelLieu = new JLabel("Lieu de la maintenance :");
			labelDate = new JLabel(" Date :");
			labelType = new JLabel("Type : ");
			labelID = new JLabel("ID : ");
			
			

		

			// ajout des composants sur le container
			
			containerPanel.add(labelID);
			containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
			containerPanel.add(textFieldID);
			containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));
			
			containerPanel.add(labelNom);
			containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
			containerPanel.add(textFieldNom);
			containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));

			containerPanel.add(labelPrix);
			containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
			containerPanel.add(textFieldPrix);
			containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));

			containerPanel.add(labelFrequence);
			containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
			containerPanel.add(textFieldFrequence);
			containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));
			
			containerPanel.add(labelLieu);
			containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
			containerPanel.add(textFieldLieu);
			containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));
			
			containerPanel.add(labelDate);
			containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
			containerPanel.add(textFieldDate);
			containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));
			
			containerPanel.add(labelType);
			containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
			containerPanel.add(textFieldType);
			containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));
			
			
			
			

			containerPanel.add(boutonValider);
			containerPanel.add(Box.createRigidArea(new Dimension(150, 10)));

			containerPanel.add(boutonAnnuler);
			containerPanel.add(Box.createRigidArea(new Dimension(150, 10)));
			
			containerPanel.add(boutonRetour);
			containerPanel.add(Box.createRigidArea(new Dimension(150, 5)));

			
		
			
			
			// ajouter une bordure vide de taille constante autour de l'ensemble des
			// composants
			containerPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

			// ajout des ecouteurs sur les boutons pour gerer les evenements
			boutonValider.addActionListener(this);
			boutonAnnuler.addActionListener(this);
			boutonRetour.addActionListener(this);


			// permet de quitter l'application si on ferme la fenetre
			this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

			this.setContentPane(containerPanel);

			// affichage de la fenetre
			this.setVisible(true);
			}
	

		/**
		 * Gere les actions realisees sur les boutons
		 *
		 */
		public void actionPerformed(ActionEvent ae) {
			int retour; // code de retour de la classe ContratDAO

			try {
				if (ae.getSource() == boutonValider) {
					// on cr�e l'objet message
					Contrat a = new Contrat(
							this.textFieldID.getText(),
							this.textFieldNom.getText(),
							Double.parseDouble(this.textFieldPrix.getText()),
							this.textFieldFrequence.getText(),
							this.textFieldLieu.getText(),
							this.textFieldDate.getText(),
							this.textFieldType.getText());
					// on demande � la classe de communication d'envoyer l'article
					// dans la table article
					retour = monContratDAO.ajouter(a);
					// affichage du nombre de lignes ajout�es
					// dans la bdd pour v�rification
					System.out.println("" + retour + " Contrat ajout�e ");
					if (retour == 1)
						JOptionPane.showMessageDialog(this, "Contrat ajout� !");
					else
						JOptionPane.showMessageDialog(this, "erreur ajout article",
								"Erreur", JOptionPane.ERROR_MESSAGE);
					
					this.setVisible(false);
					containerPanel.removeAll();
					
					
									
				} else if (ae.getSource() == boutonAnnuler) {
					containerPanel.removeAll();
					textFieldID.setText("");
					textFieldNom.setText("");
					textFieldPrix.setText("");
					textFieldFrequence.setText("");
					textFieldLieu.setText("");
					textFieldDate.setText(""); 
					textFieldType.setText("");
					
		

}
			
			
			else if (ae.getSource()== boutonRetour){
				containerPanel.removeAll();
				this.setVisible(false);
				new FenetreContrat();
			}
			
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this,
					"Veuillez controler vos saisies", "Erreur",
					JOptionPane.ERROR_MESSAGE);
			System.err.println("Veuillez controler vos saisies");
		}

	}

	public static void main(String[] args) {
		//new FenetreCreerClient();
	}

}
			
